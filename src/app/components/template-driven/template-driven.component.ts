import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-template-driven',
  templateUrl: './template-driven.component.html',
  styleUrls: ['./template-driven.component.css']
})
export class TemplateDrivenComponent implements OnInit {

  @ViewChild('myForm') templateForm;
  informacoes = {
    nome:'',
    idade:'',
    email:'',
    confirmaEmail:''
  }

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
  }

  onSubmit(myForm: NgForm){ console.log(myForm.value);}

  getAddress(cep){
    this.httpClient.get(`http://viacep.com.br/ws/${cep}/json`)
      .subscribe(
        endereco =>{
          this.templateForm.form.patchValue({endereco});
        }
      );
  }

}
